var prefix = "/activiti"
$(function() {
	//selectedHtml("classify","1B586792B420480381F9BE36617334F8", $("#classifyValue").val());
	load(); 
});

function load() {
    $('#dynamic-table')
        .bootstrapTable(
            {
                method : 'post', // 服务器数据的请求方式 get or post
                url : prefix + "/updatetasklist", // 服务器数据的加载地址
                showRefresh : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                singleSelect : false, // 设置为true将禁止多选
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                // search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
                queryParamsType : "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams : function(params) {
                	console.log(params);
                    return {
                        // 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber : params.pageNumber,
                        pageSize : params.pageSize,
//                        albumName : $('#name').val(),
//                        classify : $('#classify').val()
                    };
                },
                // 返回false将会终止请求
                responseHandler : function(res){
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns : [
					{ // 列配置项
						// 数据类型，详细参数配置参见文档http://bootstrap-table.wenzhixin.net.cn/zh-cn/documentation/
						checkbox : true,
						width : '36px'
					// 列表中显示复选框
					},
                    {
                        title : '申请人id',
                        field : 'user_id',
                        align : 'center',
                        valign : 'middle',
                        width : '20%' 
                    },
                    {
                        field : 'leave_type',
                        title : '申请类型' 
                    },
                    {
                        field : 'start_time',
                        title : '开始时间' 
                    },
                    {
                        field : 'end_time',
                        title : '结束时间' 
                    },
                    {
                        field : 'reason',
                        title : '原因' 
                    },
                    {
                        field : 'taskid',
                        title : '任务id' 
                    },
                    {
                        field : 'taskname',
                        title : '任务名称' 
                    },
                    {
                        field : 'process_instance_id',
                        title : '流程实例id' 
                    },
                    {
                        field : 'taskcreatetime',
                        title : '任务创建时间' 
                    }, 
                    {
                        title : '操作',
                        field : 'id',
                        align : 'center',
                        width : '180px',
                        formatter : function(item, row,index) { 
                            var d = '<a class="btn btn-info btn-sm href="#" title="处理"  mce_href="#" onclick="deal(\''
                                + row.taskid
                                + '\')"><i class="fa fa-eye"></i></a> ';
                           
                            return  d ;
                        }
                    } ]
            });

}
function reLoad() {
    $('#dynamic-table').bootstrapTable('refresh');
}
function deal(taskId) {
	var idex = layer.open({
		type : 2,
		title : '增加',
		maxmin : true,
		shadeClose : false, // 点击遮罩关闭层
		area : [ '800px', '520px' ],
		content : prefix + '/modifyapplytask/'+taskId
	});
    layer.full(idex);
}

function add(pId) {
	var idex = layer.open({
		type : 2,
		title : '增加',
		maxmin : true,
		shadeClose : false, // 点击遮罩关闭层
		area : [ '800px', '520px' ],
		content : prefix + '/add'
	});
    layer.full(idex);
}
 
function editMsg(id) {
    var idex = layer.open({
		type : 2,
		title : '编辑',
		maxmin : true,
		shadeClose : false, // 点击遮罩关闭层
		area : [ '800px', '520px' ],
		content : prefix + '/editMsg/' + id // iframe的url
	});
    layer.full(idex);
}

function view(id) {
    var idex = layer.open({
		type : 2,
		title : '查看',
		maxmin : true,
		shadeClose : false, // 点击遮罩关闭层
		area : [ '800px', '520px' ],
		content : '/album/photo?albumId=' + id // iframe的url
	});
    layer.full(idex);
}

function removeone(id) {
	layer.confirm('确定要删除选中的记录？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : prefix + "/deletedeploy",
			type : "post",
			data : {
				'deployid' : id
			},
			success : function(r) {
				console.log(r);
				if (r.code == 200) {
					layer.msg(r.msg);
					reLoad();
				} else {
					layer.msg(r.msg);
				}
			}
		});
	})
}

function batchRemove() {
	var rows = $('#dynamic-table').bootstrapTable('getSelections'); // 返回所有选择的行，当没有选择的记录时，返回一个空数组
	if (rows.length == 0) {
		layer.msg("请选择要删除的数据");
		return;
	}
	layer.confirm("确认要删除选中的'" + rows.length + "'条数据吗?", {
		btn : [ '确定', '取消' ]
	// 按钮
	}, function() {
		var ids = new Array();
		// 遍历所有选择的行数据，取每条数据对应的ID
		$.each(rows, function(i, row) {
			ids[i] = row['photoId'];
		}); 
		
		$.ajax({
			type : 'POST',
			data : {
				"ids" : ids
			},
			url : prefix + '/batchRemove',
			success : function(r) {
				if (r.code == 0) {
					layer.msg(r.msg);
					reLoad();
				} else {
					layer.msg(r.msg);
				}
			}
		});
	}, function() {});
}   
 