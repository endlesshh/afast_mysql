var prefix = "/activiti"
$(function() { 
	load(); 
});

function load() {
    $('#dynamic-table')
        .bootstrapTable(
            {
                method : 'post', // 服务器数据的请求方式 get or post
                url : prefix + "/getrunningprocesslists", // 服务器数据的加载地址
                showRefresh : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                singleSelect : false, // 设置为true将禁止多选
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                // search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
                queryParamsType : "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams : function(params) {
                	console.log(params);
                    return {
                        // 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber : params.pageNumber,
                        pageSize : params.pageSize,
//                        albumName : $('#name').val(),
//                        classify : $('#classify').val()
                    };
                },
                // 返回false将会终止请求
                responseHandler : function(res){
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns : [
					{ // 列配置项
						// 数据类型，详细参数配置参见文档http://bootstrap-table.wenzhixin.net.cn/zh-cn/documentation/
						checkbox : true,
						width : '36px'
					// 列表中显示复选框
					},
                    {
                        title : '执行ID',
                        field : 'executionid',
                        align : 'center',
                        valign : 'middle',
                        width : '20%' 
                    },
                    {
                        field : 'processInstanceid',
                        title : '流程实例id' 
                    },
                    {
                        field : 'businesskey',
                        title : '业务标识key' 
                    },
                    {
                        field : 'activityid',
                        title : '流程图id' 
                    } ]
            });

}
function reLoad() {
    $('#dynamic-table').bootstrapTable('refresh');
}