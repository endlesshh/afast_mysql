/**
 * 
 */
package com.ifast.common.utils.alioss;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import lombok.extern.slf4j.Slf4j;


/**
 * 项目名称：afast 类名称：OSSClientUtil 类描述： 创建人：Administrator 创建时间：2017年6月2日
 * 上午11:46:03 修改人：Administrator 修改时间：2017年6月2日 上午11:46:03 修改备注：
 * 
 * @version
 */
@Slf4j
public class OSSMtsUtil {
	// 阿里云API的密钥Access Key ID
	public final static String accessKeyId = "";
	// 阿里云API的密钥Access Key Secret
	public final static String accessKeySecret = "";
	//客户端连接
	private static DefaultAcsClient client = null;
	// 空间 阿里云API的bucket名称   新闻使用
	public final static String BUCKETNAME= "pinganfubeijing";
	//locatin   新闻使用
	public final static String LOCATION= "oss-cn-beijing";
	//视屏处理模板id    新闻使用
	public final static String TEMPLETID = "";
	//视屏处理通道id     新闻使用
	public final static String PIPELINEId = "";
	// 新闻使用
	public final static String ALIYUN_URL="http://beijing.oss-cn-beijing.aliyuncs.com/";
	// 新闻使用
	 
	//获取client
	public static DefaultAcsClient getClient(){
		DefaultProfile profile = DefaultProfile.getProfile(
                "cn-beijing",
                accessKeyId,
                accessKeySecret);
		try {
			DefaultProfile.addEndpoint("cn-beijing",
			        "cn-beijing",
			        "Mts",
			        "mts.cn-beijing.aliyuncs.com");
		} catch (ClientException e1) {
			log.info(e1.toString());
		}
		synchronized (OSSMtsUtil.class) {
			if (client == null){
				client = new DefaultAcsClient(profile);
			}
		}
		return client;
	}

}
