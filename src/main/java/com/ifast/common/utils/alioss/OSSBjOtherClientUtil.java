/**
 * 
 */
package com.ifast.common.utils.alioss;

import com.aliyun.oss.OSSClient;
import com.xiaoleilu.hutool.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.util.Map;

/**
 * 项目名称：afast 类名称：OSSClientUtil 类描述： 创建人：Administrator 创建时间：2017年6月2日
 * 上午11:46:03 修改人：Administrator 修改时间：2017年6月2日 上午11:46:03 修改备注：
 * 
 * @version
 */
@Slf4j
public class OSSBjOtherClientUtil {
	// 阿里云API的内或外网域名
	private final static String ENDPOINT = "http://oss-cn-beijing.aliyuncs.com";
	// 阿里云API的密钥Access Key ID
	private final static String ACCESSKEYID = "";
	// 阿里云API的密钥Access Key Secret
	private final static String ACCESSKEYSECRET = "";
	// 空间 阿里云API的bucket名称视频
	private final static String BUCKETNAMEVEDIO= "beijing";

	private static OSSBjOtherClientUtil instance;
	private OSSClient ossClient;

	public static OSSBjOtherClientUtil getInstance() {
		instance = new OSSBjOtherClientUtil();
		return instance;
	}

	public OSSBjOtherClientUtil() {
		ossClient = new OSSClient(ENDPOINT, ACCESSKEYID, ACCESSKEYSECRET);
	}

	/**
	 * 上传图片
	 * @param request
	 * @param basePath
	 * @param decssPicFlag
     * @return
     */
	public UploadResult uploadImg2OssMorePic(HttpServletRequest request,
                                             String basePath, boolean decssPicFlag, String style) {
		OssCommClientUtil ossCommClientUtil =  OssCommClientUtil.getInstance();
		return ossCommClientUtil.uploadImg2OssMorePic(request, basePath, BUCKETNAMEVEDIO, ossClient,decssPicFlag,style);
	}

	/**
	 * 上传文件
	 * @param request
	 * @param basePath
     * @return
     */
	public UploadResult uploadImg2OssMoreFile(HttpServletRequest request,
                                              String basePath) {
		OssCommClientUtil ossCommClientUtil =  OssCommClientUtil.getInstance();
		return ossCommClientUtil.uploadImg2OssMoreFile(request, basePath, BUCKETNAMEVEDIO, ossClient);
	}

	/**
	 * 基于流,上传图片
	 * @param basePath
	 * @return
	 */
	public UploadResult uploadImg2OssMorePicForStream(InputStream instream, String fileName,
                                                      String basePath) {
		UploadResult uploadResult = new UploadResult();
		try{
			OssCommClientUtil ossCommClientUtil =  OssCommClientUtil.getInstance();
			Map<String,String> maps =  ossCommClientUtil.uploadFile2OSSForStream(instream,fileName, basePath, "pic",BUCKETNAMEVEDIO, ossClient);
			String md5 = maps.get("ret");
			String url = maps.get("url");
			log.info("--------------------文件上传开始uploadImg2OssOnePic="+url);
			log.info("--------------------文件上传开始uploadImg2OssOnePic="+md5);
			if (StrUtil.isBlank(md5)) {
				uploadResult.setSuccess(false);
				uploadResult.setMessage("图片上传失败！");
				return uploadResult;
			}
			// 获取回掉地址
			String returnUrl = this.getUrl(url);
			uploadResult.getFileURLs().add(returnUrl);
		} catch (Exception e) {
			uploadResult.setSuccess(false);
			uploadResult.setMessage("图片上传失败！");
			uploadResult.getFileURLs().clear();
			log.error("图片上传失败！" + e.toString());
		}
		ossClient.shutdown();
		return uploadResult;
	}

	/**
	 * 基于字节,上传图片
	 * @param basePath
	 * @return
	 */
	public UploadResult uploadImg2OssMorePicForByte(byte[] content, String fileName,
                                                    String basePath) {
		UploadResult uploadResult = new UploadResult();
		try{
			OssCommClientUtil ossCommClientUtil =  OssCommClientUtil.getInstance();
			Map<String,String> maps =  ossCommClientUtil.uploadFile2OSSForByte(content,fileName, basePath, "pic",BUCKETNAMEVEDIO, ossClient);
			String md5 = maps.get("ret");
			String url = maps.get("url");
			log.info("--------------------文件上传开始uploadImg2OssOnePic="+url);
			log.info("--------------------文件上传开始uploadImg2OssOnePic="+md5);
			if (StrUtil.isBlank(md5)) {
				uploadResult.setSuccess(false);
				uploadResult.setMessage("图片上传失败！");
				return uploadResult;
			}
			// 获取回掉地址
			String returnUrl = this.getUrl(url);
			uploadResult.getFileURLs().add(returnUrl);
		} catch (Exception e) {
			uploadResult.setSuccess(false);
			uploadResult.setMessage("图片上传失败！");
			uploadResult.getFileURLs().clear();
			log.error("图片上传失败！" + e.toString());
		}
		ossClient.shutdown();
		return uploadResult;
	}
	public boolean deletePic(String url) { 
		return OssCommClientUtil.getInstance().deletePic(url, BUCKETNAMEVEDIO, ossClient).isSuccess();
	}
	/**
	 * 获取ali的连接 文件
	 * @param key
	 * @return
     */
	public String getUrlFile(String key) {
		OssCommClientUtil ossCommClientUtil =  OssCommClientUtil.getInstance();
		return ossCommClientUtil.getUrlFile(key, BUCKETNAMEVEDIO, ossClient);
	}

	/**
	 * 获取ali的连接 图片
	 * @param key
	 * @return
     */
	public String getUrl(String key) {
		OssCommClientUtil ossCommClientUtil = OssCommClientUtil.getInstance();
		return ossCommClientUtil.getUrl(key, BUCKETNAMEVEDIO, ossClient,true,"");
	}
	
}
