package com.ifast.common.config;

import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomActivitiCfgConfigurer implements ProcessEngineConfigurationConfigurer {
	//解决activiti流程图中文乱码问题
    @Override
    public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {
        processEngineConfiguration.setActivityFontName("宋体");
        processEngineConfiguration.setLabelFontName("宋体"); 
        processEngineConfiguration.setAnnotationFontName("宋体");
 
    }
}