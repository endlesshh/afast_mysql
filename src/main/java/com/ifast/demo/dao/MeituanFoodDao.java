package com.ifast.demo.dao;

import com.ifast.common.base.BaseDao;
import com.ifast.demo.domain.MeituanFoodDO;

/**
 * 
 * <pre>
 * 基础表
 * </pre>
 * <small> 2018-07-27 23:38:24 | Aron</small>
 */
public interface MeituanFoodDao extends BaseDao<MeituanFoodDO> {

}
