package com.ifast.demo.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
@TableName("spider_meituan_hotel_shop")
@Data
@EqualsAndHashCode(callSuper=false)
public class MeituanHotelDO extends Model<MeituanHotelDO> implements Serializable { 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1758012973287469103L;
	// 编号
    @TableId
    private Long id;
	private String dayRoomSpan;  
	private String dist; 	 
	private int cityId;       
	private int historyCouponCount;    
	private String hotelStar;   
	private String avgScore;	  
	private String areaName;	  
	private boolean hasGroup;	  
	private String showType;            
	private Long shopId;         
	private boolean withoutAds;	
	private double lat;	
	private double lng;
	private String roomType;
	private String cates;
	private String recommedReason;
	private int hourRoomSpan;
	private boolean merchantMember;
	private String poiSaleAndSpanTag;
	private String poiRecommendTag;
	private boolean dpSuperHotel;
	private String cooperationInfo;
	private int sourceType;
	private String frontImg;
	private int brandId;
	private int poiType;
	private String name;
	private double notRoundedOriginalPrice;
	private Long poiid;
	private int isCooperated;
	private String commentsCountDesc;
	private double lowestPrice;
	private boolean hasPackage;
	private double originalPrice;
	private boolean panoramaAvailable;
	private String ugcInfo;
	private boolean showOriginalPrice;
	private String cityName;
	private String videoUrl;
	private String scoreIntro;
	private int hotelAppointmentExtType;
	private String useTime;
	private String poiLastOrderTime;
	private double taxFee;
	private String addr;
	private String historySaleCount;
	private String posdescr;
	private int priceType;
	private String originalPriceDesc;
	private int zlSourceType;
	private Long areaId;
	private boolean flagshipFlag; 
	private boolean realPoiId;
	private double notRoundedLowestPrice;
	private int markNumbers; 
	
	@Override
	protected Serializable pkVal() { 
		return id;
	} 
}
