package com.ifast.demo.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
@TableName("spider_meituan_hotel_license")
@Data
@EqualsAndHashCode(callSuper=false)
public class MeitTuanLicenseHotelDO extends Model<MeitTuanLicenseHotelDO> implements Serializable {
	 
	private static final long serialVersionUID = 3892549069386969809L;
	// 编号
    @TableId
    private Long id;
    private int poiId;  //店铺id
	private String level;  //执照等级
	private String name;
	private String licenseNo;
	private String legalRepresentative;
	private String address;
	private String scope;
	private String validDate;
	private String businessLicenceImgUrl;
	private String restaurantLicenceImgUrl;
	private String businessLicenceImgPath;
	private String restaurantLicenceImgPath;
	
	@Override
	protected Serializable pkVal() { 
		return id;
	} 
}
