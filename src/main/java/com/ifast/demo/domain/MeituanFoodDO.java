package com.ifast.demo.domain;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
@TableName("spider_meituan_food_shop")
@Data
@EqualsAndHashCode(callSuper=false)
public class MeituanFoodDO extends Model<MeituanFoodDO> implements Serializable {
	 
	private static final long serialVersionUID = 3740323679558281308L;
	// 编号
    @TableId
    private Long id;
	private String adsClickUrl;  //广告链接
	private boolean hasAds; 	 //是否有广告
	private String address;      //地址
	private double avgScore;     //平均分
	private int allCommentNum;   //评论条数
	private String adsShowUrl;	 //广告展示路径
	private String frontImg;	 //门店图片
	private double avgPrice;	 //人均消费
	private int poiId;           //店铺id
	private String title;        //店铺名称
	//private List<String> dealList;//详情list
	
	 
	@Override
	protected Serializable pkVal() { 
		return id;
	} 
}
