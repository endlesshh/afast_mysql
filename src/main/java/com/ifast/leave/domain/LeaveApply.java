package com.ifast.leave.domain;

import java.io.Serializable;

import org.activiti.engine.task.Task;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("请假表")
@TableName("TB_LEAVE_APPLY") 
@Data
public class LeaveApply  extends Model<LeaveApply> implements Serializable{
	@ApiModelProperty("主键")
	@TableId(type= IdType.ID_WORKER)
	long id;
	@ApiModelProperty("流程实例id")
	String processInstanceId;
	@ApiModelProperty("用户名")
	String userId;
	@ApiModelProperty("请假起始时间")
	String startTime;
	@ApiModelProperty("请假结束时间")
	String endTime;
	@ApiModelProperty("请假类型")
	String leaveType;
	@ApiModelProperty("请假原因")
	String reason;
	@ApiModelProperty("申请时间")
	String applyTime;
	@ApiModelProperty("实际请假起始时间")
	String realityStartTime;
	@ApiModelProperty("实际请假结束时间")
	String realityEndTime;
	@TableField(exist=false)
	Task task;
	 
	@Override
	protected Serializable pkVal() { 
		return this.id;
	} 
}
