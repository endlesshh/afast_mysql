package com.ifast.leave.dao;

import com.ifast.common.base.BaseDao;
import com.ifast.leave.domain.LeaveApply;

public interface LeaveApplyDao extends BaseDao<LeaveApply>{
	void save(LeaveApply apply);

	LeaveApply getLeaveApply(int id);

	int updateByPrimaryKey(LeaveApply record);
}
