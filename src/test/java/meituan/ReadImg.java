package meituan;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
 
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;
 
public class ReadImg {
 
	 public static void main(String[] args) throws MalformedURLException {
	        String url = "http://s3plus-img.sankuai.com/skynet/0000701BB716724AE84CC0BB729E67DF.jpeg%40600h%7Cwatermark%3D1%26align%3D1%26x%3D20%26object%3Dd2F0ZXJtYXJrLTIwMTYxMTEwMS5wbmc%3D%0A%26p%3D5%26t%3D15%26order%3D0?AWSAccessKeyId=c64763be32f940a49e91fac6670f2fff&Expires=1586770572&Signature=jS5cTBaudgZPl4%2BX9vOzVzFwgzw%3D";
	        String path="E:/face/img/pic.jpeg";
	        downloadPicture(url,path);
	    }
	    //链接url下载图片
	 private static void downloadPicture(String urlList,String path) throws MalformedURLException { 
	    	URL url = new URL(urlList);
	        try (DataInputStream dataInputStream = new DataInputStream(url.openStream());
	        	 FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
		         ByteArrayOutputStream output = new ByteArrayOutputStream();){ 
	            byte[] buffer = new byte[1024];
	            int length; 
	            while ((length = dataInputStream.read(buffer)) > 0) {
	                output.write(buffer, 0, length);
	            }
	            fileOutputStream.write(output.toByteArray()); 
	        } catch (Exception e) {
	            e.printStackTrace();
	        } 
	    } 
}
 