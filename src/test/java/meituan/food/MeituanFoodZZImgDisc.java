package meituan.food;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.google.common.collect.Lists;
import com.ifast.Application;
import com.ifast.common.utils.WorkId;
import com.ifast.demo.domain.MeitTuanLicenseFoodDO;
import com.ifast.demo.domain.MeituanFoodDO;

import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.model.HttpRequestBody;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.utils.HttpConstant;

/**
 *  
 * @author ShiQiang
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes={Application.class}) 
public class MeituanFoodZZImgDisc {
	protected String path = "E:\\face\\img\\";
	public static String PATH_SEPERATOR = File.separator;
	
	@Test
	public void testImport()throws Exception{
		
		String path = this.path + PATH_SEPERATOR + "chifeng.meituan.com" + PATH_SEPERATOR;
		MeitTuanLicenseFoodDO  lices = new MeitTuanLicenseFoodDO();
		List<MeitTuanLicenseFoodDO> foods = lices.selectList(new EntityWrapper<>().isNotNull("businessLicenceImgUrl").and().isNull("businessLicenceImgPath").orNew().isNotNull("restaurantLicenceImgUrl").and().isNull("restaurantLicenceImgPath"));
		System.out.println(foods);
		//	       try {
//	       
//				if(lice != null) { 
//					String firstName = WorkId.sortUID();
//					if(StringUtils.isNotBlank(lice.getBusinessLicenceImgUrl())){
//						String name = firstName + "_business" + findImgSuffix(lice.getBusinessLicenceImgUrl());
//						downloadPicture(lice.getBusinessLicenceImgUrl(),path + name);
//						lice.setBusinessLicenceImgPath(task.getUUID() + PATH_SEPERATOR+name);
//					}
//					if(StringUtils.isNotBlank(lice.getRestaurantLicenceImgUrl())){
//						String name = firstName + "_restaurant" + findImgSuffix(lice.getRestaurantLicenceImgUrl());
//						downloadPicture(lice.getRestaurantLicenceImgUrl(),path + name);
//						lice.setRestaurantLicenceImgPath(task.getUUID() + PATH_SEPERATOR +name);
//					} 
//					lice.updateById();
//				} 
//	       } catch (Exception e) {
//	           e.printStackTrace();
//	       }
   }
   public static void main(String[] args) {
    
	} 
   
   private String findImgSuffix(String imgUrl){ 
	   return imgUrl.indexOf(".jpeg") > 0 ? ".jpeg": imgUrl.indexOf(".jpg") > 0 ? ".jpg":imgUrl.indexOf(".png") > 0 ? ".png":".jpg";
   }
   
   private static void downloadPicture(String urlList,String path) throws MalformedURLException { 
   	URL url = new URL(urlList);
       try (DataInputStream dataInputStream = new DataInputStream(url.openStream());
       	 FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
	         ByteArrayOutputStream output = new ByteArrayOutputStream();){ 
       	
           byte[] buffer = new byte[1024];
           int length; 
           while ((length = dataInputStream.read(buffer)) > 0) {
               output.write(buffer, 0, length);
           }
           fileOutputStream.write(output.toByteArray()); 
           
       } catch (Exception e) {
           e.printStackTrace();
       } 
   }
}
