package meituan.food;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;
import com.ifast.Application;
import com.ifast.demo.domain.MeitTuanLicenseFoodDO;
import com.ifast.demo.domain.MeituanFoodDO;

import meituan.FilePipeline;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.model.HttpRequestBody;
import us.codecraft.webmagic.pipeline.Pipeline;
import us.codecraft.webmagic.utils.HttpConstant;

/**
 *  
 * @author ShiQiang
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes={Application.class}) 
public class MeituanFoodLicenseDB {
 
	@Test
	public void testImport()throws Exception{
		MeituanFoodDO food = new MeituanFoodDO();
		List<MeituanFoodDO> allFoods = food.selectAll();
		
		List<Request> reqs = Lists.newArrayList();
		allFoods.stream().forEach(one -> {
			Request req = new Request("https://chifeng.meituan.com/meishi/api/poi/getFoodSafetyDetail");  
			req.setMethod(HttpConstant.Method.POST).setRequestBody(HttpRequestBody.json("{\"poiId\":\""+one.getPoiId()+"\"}", "UTF-8"));
			req.putExtra("poiId", one.getPoiId());
			reqs.add(req);
		});  
		
    	Spider.create(new MeituanFoodLicenseProcessor()).addPipeline(new Pipeline() {

			@Override
			public void process(ResultItems resultItems, Task task) {
				MeitTuanLicenseFoodDO lice = resultItems.get(MeituanFoodLicenseProcessor.key);
				if(lice != null) {
					lice.insert(); 
				}
			} 
        }).addPipeline(new FilePipeline("E:\\face\\img","food")).addRequest(reqs.toArray(new Request[reqs.size()])).run(); 
        /*
    	Request loing = new Request("https://chifeng.meituan.com/meishi/api/poi/getFoodSafetyDetail");  
    	loing.putExtra("poiId", "164844118");
    	loing.setMethod(HttpConstant.Method.POST).setRequestBody(HttpRequestBody.json("{\"poiId\":\"164844118\"}", "UTF-8"));
        Spider.create(new MeituanFoodZZProcessor()).addRequest(loing).addPipeline(new Pipeline() {

			@Override
			public void process(ResultItems resultItems, Task task) {
				MeitTuanLicense lice = resultItems.get(MeituanFoodZZProcessor.key);
				if(lice != null) {
					lice.insert(); 
				}
			} 
        }).addPipeline(new FilePipeline("E:\\face\\img")).run();*/
	} 
	 
}
