package meituan.food;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.ifast.Application;
import com.ifast.demo.domain.MeituanFoodDO;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 *  
 * @author ShiQiang
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes={Application.class}) 
public class MeituanFoodDB {
 
	@Test
	public void testImport()throws Exception{
		int total = 324;
    	int i = 1;
    	List<String> urls = new ArrayList<String>();
    	while(total >=0 ){
    		urls.add("https://chifeng.meituan.com/meishi/b2368/pn"+i+"/");
    		i++;
    		total -= 15;
    	}
    	Spider.create(new MeituanFoodProcessor()).addPipeline(new Pipeline() {

			@Override
			public void process(ResultItems resultItems, Task task) {
				List<MeituanFoodDO> shops = resultItems.get(MeituanFoodProcessor.key);
				if(CollectionUtils.isNotEmpty(shops)) {
					shops.forEach(shop ->{
						shop.insert();
					});
				}
			}
        	
        }).addUrl(urls.toArray(new String[urls.size()])).run(); 
	} 
	//@Transactional
//	@Rollback(false)
//	private void insert(String res){
//		jdbcTemplate.update(res);
//	}
 
}
