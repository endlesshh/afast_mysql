package meituan;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;

import com.ifast.common.utils.WorkId;
import com.ifast.demo.domain.MeitTuanLicenseFoodDO;

import cn.hutool.core.io.FileUtil;
import cn.hutool.http.HttpUtil;
import meituan.food.MeituanFoodLicenseProcessor;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 * Store results in files.<br>
 *
 * @author code4crafter@gmail.com <br>
 * @since 0.1.0
 */
public class FilePipeline  implements Pipeline {
 
	protected String path;
	private String title;
	public static String PATH_SEPERATOR = File.separator;
	
	public FilePipeline(String path,String title) {
	    this.path = path;
	    this.title = title;
	}

    @Override
    public void process(ResultItems resultItems, Task task) {
        String path = this.path + PATH_SEPERATOR + task.getUUID() + PATH_SEPERATOR +  title  + PATH_SEPERATOR;
        if(!FileUtil.exist(path)) {
        	FileUtil.mkdir(path);
        }
        try {
        	MeitTuanLicenseFoodDO lice = resultItems.get(MeituanFoodLicenseProcessor.key);
			if(lice != null) { 
				String firstName = WorkId.sortUID();
				if(StringUtils.isNotBlank(lice.getBusinessLicenceImgUrl())){
					String name = firstName + "_business" + findImgSuffix(lice.getBusinessLicenceImgUrl());
					downloadPicture(lice.getBusinessLicenceImgUrl(),path + name);
					lice.setBusinessLicenceImgPath(task.getUUID() + PATH_SEPERATOR +  title  + PATH_SEPERATOR+name);
				}
				if(StringUtils.isNotBlank(lice.getRestaurantLicenceImgUrl())){
					String name = firstName + "_restaurant" + findImgSuffix(lice.getRestaurantLicenceImgUrl());
					downloadPicture(lice.getRestaurantLicenceImgUrl(),path + name);
					lice.setRestaurantLicenceImgPath(task.getUUID() + PATH_SEPERATOR +  title  + PATH_SEPERATOR +name);
				} 
				lice.updateById();
			} 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
    	 if(!FileUtil.exist("E:\\face\\img\\")) {
         	FileUtil.mkdir("E:\\face\\img\\");
         }
    	 HttpUtil.downloadFile("http://s3plus-img.sankuai.com/skynet/0000701BB716724AE84CC0BB729E67DF.jpeg%40600h%7Cwatermark%3D1%26align%3D1%26x%3D20%26object%3Dd2F0ZXJtYXJrLTIwMTYxMTEwMS5wbmc%3D%0A%26p%3D5%26t%3D15%26order%3D0?AWSAccessKeyId=c64763be32f940a49e91fac6670f2fff&Expires=1586770572&Signature=jS5cTBaudgZPl4%2BX9vOzVzFwgzw%3D", FileUtil.file("E:\\face\\img\\a.jpeg" ));
    	 
	} 
    
    private String findImgSuffix(String imgUrl){ 
    	return imgUrl.indexOf(".jpeg") > 0 ? ".jpeg": imgUrl.indexOf(".jpg") > 0 ? ".jpg":imgUrl.indexOf(".png") > 0 ? ".png":".jpg";
    }
    
    private static void downloadPicture(String urlList,String path) throws MalformedURLException { 
    	URL url = new URL(urlList);
        try (DataInputStream dataInputStream = new DataInputStream(url.openStream());
        	 FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
	         ByteArrayOutputStream output = new ByteArrayOutputStream();){ 
        	
            byte[] buffer = new byte[1024];
            int length; 
            while ((length = dataInputStream.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
            fileOutputStream.write(output.toByteArray()); 
            
        } catch (Exception e) {
            e.printStackTrace();
        } 
    } 
}
