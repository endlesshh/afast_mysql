package meituan.hotel;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ifast.Application;
import com.ifast.demo.domain.MeituanHotelDO;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 *  
 * @author ShiQiang
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes={Application.class}) 
public class MeituanHotelDB {
 
	@Test
	public void testImport()throws Exception{
		int total = 237;
    	int i = 0;
    	List<String> urls = Lists.newArrayList();
    	while(total >=0 ){
    		urls.add("https://ihotel.meituan.com/hbsearch/HotelSearch?utm_medium=pc&version_name=999.9&cateId=20&attr_28=129"
            		+ "&uuid=D2A4F635F921373E02C150C2C9CBF1095B30AFBA79BA5F6903E0BC827CAA62B8%401581647541457"
            		+ "&cityId=142&offset="+i+"&limit=50&startDay=20200214&endDay=20200214&q=&sort=defaults"
            		+ "&areaId=2368&X-FOR-WITH=Hx2W1T%2F%2BhbuoXRDgM40zLBgo29av4qC%2B8D2WCETrbD3Xuzzb9QK%2FwCt30HZ65MgOUED4H4P1TOEyn5xG14JxruMr2AZSV4oCEF%2F8B%2FdBE4uQRaN7MqRF4DSZi2jgEJw%2F3nrGnMfVAZ7F0jos87ScPIULFZnDr2lbxtsz3Y7awNuxDpRfpHJkw32KgdEmg8zZRPKiSHnOQgWqIMvcDgjskrl3TrfzqlxbcZ5ZlSd2WME%3D");
    		i += 50;
    		total -= 50;
    	} 
    	Spider.create(new MeituanHotelProcessor()) .addPipeline(new Pipeline() {

 			@Override
 			public void process(ResultItems resultItems, Task task) {
 				List<MeituanHotelDO> shops = resultItems.get(MeituanHotelProcessor.key);
 				if(CollectionUtils.isNotEmpty(shops)) {
 					shops.forEach(shop ->{
 						shop.insert();
 					});
 				}
 			}
         	
         }).addUrl(urls.toArray(new String[urls.size()])).run(); 
    	
    	 /*Spider.create(new MeituanHotelProcessor()).addUrl("https://ihotel.meituan.com/hbsearch/HotelSearch?utm_medium=pc&version_name=999.9&cateId=20&attr_28=129"
         		+ "&uuid=D2A4F635F921373E02C150C2C9CBF1095B30AFBA79BA5F6903E0BC827CAA62B8%401581647541457"
         		+ "&cityId=142&offset=0&limit=50&startDay=20200214&endDay=20200214&q=&sort=defaults"
         		+ "&areaId=2368&X-FOR-WITH=Hx2W1T%2F%2BhbuoXRDgM40zLBgo29av4qC%2B8D2WCETrbD3Xuzzb9QK%2FwCt30HZ65MgOUED4H4P1TOEyn5xG14JxruMr2AZSV4oCEF%2F8B%2FdBE4uQRaN7MqRF4DSZi2jgEJw%2F3nrGnMfVAZ7F0jos87ScPIULFZnDr2lbxtsz3Y7awNuxDpRfpHJkw32KgdEmg8zZRPKiSHnOQgWqIMvcDgjskrl3TrfzqlxbcZ5ZlSd2WME%3D")
    	 .addPipeline(new Pipeline() {

 			@Override
 			public void process(ResultItems resultItems, Task task) {
 				List<MeituanHotelDO> shops = resultItems.get(MeituanHotelProcessor.key);
 				if(CollectionUtils.isNotEmpty(shops)) {
 					shops.forEach(shop ->{
 						shop.insert();
 					});
 				}
 			}
         	
         }).run();*/
     
	} 
 
}
