package meituan.hotel;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.ifast.demo.domain.MeituanHotelDO;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/**
 * @author code4crafter@gmail.com <br>
 */
public class MeituanHotelProcessor implements PageProcessor {
	
	public static String key = "dbKey";
	
    @Override
    public void process(Page page) { 
       System.out.println(page.getJson()); 
       if(page.getJson() != null){
    	   JSONObject obj = JSON.parseObject(page.getJson().toString());
    	   JSONArray arr = obj.getJSONObject("data").getJSONArray("searchresult");
    	   List<MeituanHotelDO> lists = Lists.newArrayList();
    	   for(int i=0,total = arr.size();i<total;i++){ 
    		   MeituanHotelDO jobj = JSON.parseObject(arr.getString(i).toString(),MeituanHotelDO.class);
    		   System.out.println(jobj);
    		   lists.add(jobj); 
    	   } 
    	   page.putField(key, lists);
       }
    }

    @Override
    public Site getSite() {
    	Site site = Site.me().setDomain("ihotel.meituan.com");
    	site.addHeader("Content-Type","application/json");
    	site.addHeader("Host","ihotel.meituan.com"); 
    	site.addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36");
    	site.addHeader("Connection","keep-alive");
        return site; 
    }

    public static void main(String[] args){
    	
    	int total = 237;
    	int i = 0;
    	List<String> urls = new ArrayList<String>();
    	while(total >=0 ){
    		urls.add("https://ihotel.meituan.com/hbsearch/HotelSearch?utm_medium=pc&version_name=999.9&cateId=20&attr_28=129"
            		+ "&uuid=D2A4F635F921373E02C150C2C9CBF1095B30AFBA79BA5F6903E0BC827CAA62B8%401581647541457"
            		+ "&cityId=142&offset="+i+"&limit=50&startDay=20200214&endDay=20200214&q=&sort=defaults"
            		+ "&areaId=2368&X-FOR-WITH=Hx2W1T%2F%2BhbuoXRDgM40zLBgo29av4qC%2B8D2WCETrbD3Xuzzb9QK%2FwCt30HZ65MgOUED4H4P1TOEyn5xG14JxruMr2AZSV4oCEF%2F8B%2FdBE4uQRaN7MqRF4DSZi2jgEJw%2F3nrGnMfVAZ7F0jos87ScPIULFZnDr2lbxtsz3Y7awNuxDpRfpHJkw32KgdEmg8zZRPKiSHnOQgWqIMvcDgjskrl3TrfzqlxbcZ5ZlSd2WME%3D");
    		i += 50;
    		total -= 50;
    	}
    	
        Spider.create(new MeituanHotelProcessor()).addUrl("https://ihotel.meituan.com/hbsearch/HotelSearch?utm_medium=pc&version_name=999.9&cateId=20&attr_28=129"
        		+ "&uuid=D2A4F635F921373E02C150C2C9CBF1095B30AFBA79BA5F6903E0BC827CAA62B8%401581647541457"
        		+ "&cityId=142&offset=0&limit=50&startDay=20200214&endDay=20200214&q=&sort=defaults"
        		+ "&areaId=2368&X-FOR-WITH=Hx2W1T%2F%2BhbuoXRDgM40zLBgo29av4qC%2B8D2WCETrbD3Xuzzb9QK%2FwCt30HZ65MgOUED4H4P1TOEyn5xG14JxruMr2AZSV4oCEF%2F8B%2FdBE4uQRaN7MqRF4DSZi2jgEJw%2F3nrGnMfVAZ7F0jos87ScPIULFZnDr2lbxtsz3Y7awNuxDpRfpHJkw32KgdEmg8zZRPKiSHnOQgWqIMvcDgjskrl3TrfzqlxbcZ5ZlSd2WME%3D").run();
    }

}
