package meituan.hotel;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;
import com.ifast.Application;
import com.ifast.demo.domain.MeitTuanLicenseHotelDO;
import com.ifast.demo.domain.MeituanHotelDO;

import meituan.HotelFilePipeline;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

/**
 *  
 * @author ShiQiang
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes={Application.class}) 
public class MeituanHotelLicenseDB {
 
	@Test
	public void testImport()throws Exception{
		MeituanHotelDO food = new MeituanHotelDO();
		List<MeituanHotelDO> alls = food.selectAll();
		
		List<Request> reqs = Lists.newArrayList();
		alls.stream().forEach(one -> {
			Request req = new Request("https://ihotel.meituan.com/group/v1/qualification?uuid=D2A4F635F921373E02C150C2C9CBF1095B30AFBA79BA5F6903E0BC827CAA62B8&cityId=142&utm_medium=touch&version_name=999.9&poiId="+one.getPoiid());  
			req.putExtra("poiId", one.getPoiid());
			reqs.add(req);
		});  
		
    	Spider.create(new MeituanHotelLicenseProcessor()).addPipeline(new Pipeline() {

			@Override
			public void process(ResultItems resultItems, Task task) {
				MeitTuanLicenseHotelDO lice = resultItems.get(MeituanHotelLicenseProcessor.key);
				if(lice != null) {
					lice.insert(); 
				}
			} 
        }).addPipeline(new HotelFilePipeline("E:\\face\\img","hotel")).addRequest(reqs.toArray(new Request[reqs.size()])).run(); 
          
    	/*Request req = new Request("https://ihotel.meituan.com/group/v1/qualification?uuid=D2A4F635F921373E02C150C2C9CBF1095B30AFBA79BA5F6903E0BC827CAA62B8&cityId=142&utm_medium=touch&version_name=999.9&poiId=50301338");  
		req.putExtra("poiId", "50301338");
    	Spider.create(new MeituanHotelLicenseProcessor()).addPipeline(new Pipeline() { 
			@Override
			public void process(ResultItems resultItems, Task task) {
				MeitTuanLicenseHotelDO lice = resultItems.get(MeituanHotelLicenseProcessor.key);
				if(lice != null) {
					lice.insert(); 
				}
			} 
        }).addPipeline(new HotelFilePipeline("E:\\face\\img","hotel")).addRequest(req).run();*/
	} 
	 
}
