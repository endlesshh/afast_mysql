package meituan.hotel;

import org.apache.commons.collections.CollectionUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ifast.demo.domain.MeitTuanLicenseHotelDO;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Request;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

/**
 * @author code4crafter@gmail.com <br>
 */
public class MeituanHotelLicenseProcessor implements PageProcessor {
	
public static String key = "zzDbKey";
	
	@Override
    public void process(Page page) { 
		System.out.println(page.getJson()); 
		if(page.getJson() != null){
			MeitTuanLicenseHotelDO hotel = new MeitTuanLicenseHotelDO();
			hotel.setPoiId(Integer.valueOf(page.getRequest().getExtra("poiId").toString()));
			JSONObject obj = JSON.parseObject(page.getJson().toString());  
			if(CollectionUtils.isNotEmpty(obj.values())){
				hotel.setName(obj.keySet().toArray()[0].toString());
				hotel.setBusinessLicenceImgUrl(obj.values().toArray()[0].toString());
				page.putField(key, hotel);
		  	  	System.out.println(hotel);
			} 
		}
    }

    @Override
    public Site getSite() {
    	Site site = Site.me().setDomain("chifeng.meituan.com"); 
        return site;
    }

    public static void main(String[] args){ 
		Request loing = new Request("https://ihotel.meituan.com/group/v1/qualification?uuid=D2A4F635F921373E02C150C2C9CBF1095B30AFBA79BA5F6903E0BC827CAA62B8&cityId=142&utm_medium=touch&version_name=999.9&poiId=50301338");  
		loing.putExtra("poiId", "50301338"); 
        Spider.create(new MeituanHotelLicenseProcessor()).addRequest(loing).run();
    }

}
